public class Sector {


    private int x;
    private int y;
    private GameElement gameElement;

    public Sector(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public void placeElement(GameElement gameElement) {
        this.gameElement = gameElement;
    }

    public boolean isEmpty() {
        return gameElement == null;
    }

    public GameElement getGameElement(){
        return gameElement;
    }

    public void removeGameElement(){
        this.gameElement = null;
    }
}
