public class Enterprise implements GameElement{

    private Sector sector;
    private Quadrant quadrant;
    private int stardate;
    private int energy;

    public Enterprise(Quadrant quadrant, Sector sector){
        this.quadrant = quadrant;
        this.sector = sector;
        this.stardate = 30;
        this.energy = 600;
    }

    public Quadrant getQuadrant() {
        return quadrant;
    }

    public Sector getSector(){
        return sector;
    }

    public int getStardate(){
        return stardate;
    }

    public int getEnergy(){
        return energy;
    }

    public void setSector(Sector destinationSector) {
        this.sector = destinationSector;
    }

    public void setEnergy(int energy) {
        this.energy = energy;
    }

    public void setStardates(int currentStardates) {
        this.stardate = currentStardates;
    }

    public void setQuadrant(Quadrant destinationQuadrant) {
        this.quadrant = destinationQuadrant;
    }
}
