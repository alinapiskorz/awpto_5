public class KlingonsShip implements GameElement{

    private int energy;
    private Quadrant quadrant;

    public KlingonsShip(Quadrant quadrant){
        this.energy = 550;
        this.quadrant = quadrant;
    }

    public int getEnergy() {
        return energy;
    }

    public Quadrant getQuadrant() {
        return quadrant;
    }
}
