import java.util.Scanner;

public class ConsoleInterface {

    Scanner scanner = new Scanner(System.in);

    public void printStartingGame() {
        System.out.println("GRA ROZPOCZETA");
    }

    public void printEnterpriseStatus(Enterprise enterprise) {
        System.out.println(" ");
        System.out.println("** STATUS ENTERPRISE ** ENERGIA: " + enterprise.getEnergy() + " | DATY GWIEZDNE: " + enterprise.getStardate() +
                " | POZYCJA: kwadrant: [" + enterprise.getQuadrant().getX() + ", " + enterprise.getQuadrant().getY() + "] sektor: [" +
                enterprise.getSector().getX() + ", " + enterprise.getSector().getY() + "]");
    }

    public String readCommand() {
        System.out.println("** Dostepne komendy: 0, 1 **");
        System.out.println(" ");
        String command = scanner.nextLine();

        return command;
    }

    public void printCommand0() {
        System.out.println(" ");
        System.out.println("VECTOR? (x,y)");
        System.out.println(" ");
    }

    public void printCommand1(Enterprise enterprise) {
        Quadrant quadrant = enterprise.getQuadrant();

        for (int i = 7; i >=0; i--) {
            for (int j = 0; j < 8; j++) {
                Sector sector = quadrant.getSector(i, j);
                if (sector.isEmpty()) {
                    System.out.print("\t - \t");
                } else {
                    GameElement element = sector.getGameElement();
                    if (element instanceof Enterprise){
                        System.out.print("\t<*>\t");
                    } else if (element instanceof KlingonsShip){
                        System.out.print("\t+++\t");
                    } else if (element instanceof StarfleetBase){
                        System.out.print("\t>!<\t");
                    } else if (element instanceof Star){
                        System.out.print("\t * \t");
                    }
                }
            }
            System.out.println("\n");
        }
    }


    public String readVector() {
        return scanner.nextLine();
    }

    public void printFinishGame() {
        System.out.println("GRA ZAKONCZONA");
    }
}
