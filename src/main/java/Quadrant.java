import java.util.Random;

public class Quadrant {

    private int x;
    private int y;
    private int sectorsAmount = 0;
    private Sector[][] sectors = new Sector[8][8];

    public Quadrant(int x, int y){
        this.x = x;
        this.y = y;
        createSectors();
    }

    public int getX(){
        return x;
    }

    public int getY() {
        return y;
    }

    public int getSectorsAmount() {
        return this.sectorsAmount;
    }

    private void createSectors(){
        int x = 7;

        for(int i=0; i<8; i++){
            int y=0;
            for (int j=0; j<8; j++){
                sectors[i][j] = new Sector(x, y);
                sectorsAmount++;
                y++;
            }
            x--;
        }
    }

    public Sector[][] getSectors() {
        return sectors;
    }

    public Sector getSector(int x, int y) {
        Sector result = null;
        for (int i=0; i<8; i++){
            for (int j=0; j<8; j++){
                if(sectors[i][j].getX() == x){
                    if (sectors[i][j].getY() == y){
                        result = sectors[i][j];
                    }
                }
            }
        }
        return result;
    }

    public Sector getRandomSector(){
        Random rd = new Random();
        int x;
        int y;

        x = rd.nextInt(7);
        y = rd.nextInt(7);
        Sector sector = getSector(x,y);

        while(!sector.isEmpty()){
            x = rd.nextInt(7);
            y = rd.nextInt(7);
            sector = getSector(x,y);
        }

        return sector;
    }
}

