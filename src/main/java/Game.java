import java.util.ArrayList;
import java.util.List;

import static java.lang.Math.abs;

public class Game {

    private ConsoleInterface consoleInterface;
    private boolean isRunning;
    private GalaxyMap galaxyMap;
    private List<KlingonsShip> klingonsShips = new ArrayList<KlingonsShip>();
    private List<Star> stars = new ArrayList<Star>();
    private List<StarfleetBase> starfleetBases = new ArrayList<StarfleetBase>();
    private Enterprise enterprise;

    public Game() {
        consoleInterface = new ConsoleInterface();
        galaxyMap = new GalaxyMap();
        createEnterprise();
    }

    public void create() {
        isRunning = true;
        createKlingonsShips();
        createStars();
        createStarfleetBases();
        consoleInterface.printStartingGame();
    }

    public boolean isRunning() {
        return isRunning;
    }

    public void finish() {
        isRunning = false;
        consoleInterface.printFinishGame();
    }

    public int getKlingonsShipsAmount() {
        return klingonsShips.size();
    }

    public int getStarfleetBasesAmount() {
        return starfleetBases.size();
    }

    public int getStarsAmount() {
        return stars.size();
    }

    public boolean haveEnterprise() {
        return enterprise != null;
    }

    public Enterprise getEnterprise() {
        return enterprise;
    }

    public void executeMove(int x, int y) {
        int currentSectorX = enterprise.getSector().getX();
        int currentSectorY = enterprise.getSector().getY();
        int currentQuadrantX = enterprise.getQuadrant().getX();
        int currentQuadrantY = enterprise.getQuadrant().getY();
        int currentStardates = enterprise.getStardate();

        Sector currentSector = enterprise.getSector();

        if (x>0) {
            for (int i = 0; i < x; i++) {
                if (currentSectorX == 7) {
                    currentSectorX = 0;
                    currentQuadrantX++;
                    currentStardates--;
                } else {
                    currentSectorX++;
                }
            }
        }

        if (x<0) {
            for (int i = 0; i < abs(x); i++) {
                if (currentSectorX == 0) {
                    currentSectorX = 7;
                    currentQuadrantX--;
                    currentStardates--;
                } else {
                    currentSectorX--;
                }
            }
        }

        if (y>0) {
            for (int i = 0; i < y; i++) {
                if (currentSectorY == 7) {
                    currentSectorY = 0;
                    currentQuadrantY++;
                    currentStardates--;
                } else {
                    currentSectorY++;
                }
            }
        }

        if (y<0) {
            for (int i = 0; i < abs(y); i++) {
                if (currentSectorY == 0) {
                    currentSectorY = 7;
                    currentQuadrantY--;
                    currentStardates--;
                } else {
                    currentSectorY--;
                }
            }
        }

        Quadrant destinationQuadrant = galaxyMap.getQuadrant(currentQuadrantY, currentQuadrantX);

        Sector destinationSector = destinationQuadrant.getSector(currentSectorY, currentSectorX);

        if (!destinationSector.isEmpty()) {
            finish();
            return;
        }

        currentSector.removeGameElement();
        enterprise.setQuadrant(destinationQuadrant);
        enterprise.setSector(destinationSector);
        destinationSector.placeElement(enterprise);

        int distance = abs(x) + abs(y);
        enterprise.setEnergy(enterprise.getEnergy() - distance);

        if (isEnterpriseNearStarfleetBase()){
            enterprise.setEnergy(600);
        }

        enterprise.setStardates(currentStardates);

        if (enterprise.getStardate() == 0){
            for (KlingonsShip klingonsShip :klingonsShips) {
                if(klingonsShip.getQuadrant() != enterprise.getQuadrant()){
                    finish();
                }
            }
        }
    }

    public GalaxyMap getGalaxyMap() {
        return galaxyMap;
    }

    public void run() {
        while (isRunning){
            consoleInterface.printEnterpriseStatus(enterprise);
            String command = consoleInterface.readCommand();

            switch (command){
                case "0":
                    consoleInterface.printCommand0();

                    String vector = consoleInterface.readVector();
                    int x = Integer.parseInt(vector.split(",")[0]);
                    int y = Integer.parseInt(vector.split(",")[1]);

                    executeMove(x, y);

                    break;
                case "1":
                    consoleInterface.printCommand1(enterprise);

                    break;
                default:

            }

        }
    }

    private void createKlingonsShips() {
        for (int i = 0; i < 7; i++) {
            Quadrant quadrant = galaxyMap.getRandomQuadrant();
            Sector sector = quadrant.getRandomSector();

            KlingonsShip klingonsShip = new KlingonsShip(quadrant);

            sector.placeElement(klingonsShip);
            klingonsShips.add(klingonsShip);
        }
    }

    private void createStars() {
        for (int i = 0; i < 20; i++) {
            Star star = new Star();

            Quadrant quadrant = galaxyMap.getRandomQuadrant();
            Sector sector = quadrant.getRandomSector();

            sector.placeElement(star);
            stars.add(star);
        }
    }

    private void createStarfleetBases() {
        for (int i = 0; i < 2; i++) {
            StarfleetBase starfleetBase = new StarfleetBase();

            Quadrant quadrant = galaxyMap.getRandomQuadrant();
            Sector sector = quadrant.getRandomSector();

            sector.placeElement(starfleetBase);
            starfleetBases.add(starfleetBase);
        }
    }

    private void createEnterprise() {
        Quadrant quadrant = galaxyMap.getQuadrant(4, 4);
        Sector sector = quadrant.getSector(4, 4);

        enterprise = new Enterprise(quadrant, sector);

        sector.placeElement(enterprise);
    }

    private boolean isEnterpriseNearStarfleetBase() {
        Quadrant quadrant = enterprise.getQuadrant();
        Sector sector = enterprise.getSector();

        Sector upperSector = quadrant.getSector(sector.getX()+1, sector.getY());
        Sector lowerSector = quadrant.getSector(sector.getX()-1, sector.getY());
        Sector leftSector = quadrant.getSector(sector.getX(), sector.getY() - 1);
        Sector rightSector = quadrant.getSector(sector.getX(), sector.getY() + 1);

        if (upperSector != null && !upperSector.isEmpty()) {
            return upperSector.getGameElement() instanceof StarfleetBase;
        } else if (lowerSector != null && !lowerSector.isEmpty()) {
            return lowerSector.getGameElement() instanceof StarfleetBase;
        } else if (leftSector != null && !leftSector.isEmpty()) {
            return leftSector.getGameElement() instanceof StarfleetBase;
        } else if (rightSector != null && !rightSector.isEmpty()) {
            return rightSector.getGameElement() instanceof StarfleetBase;
        }
        return false;
    }
}
