import java.util.Random;

public class GalaxyMap {

    private Quadrant[][] quadrants = new Quadrant[8][8];

    public GalaxyMap(){
        createQuadrants();
    }

    public Quadrant[][] getQuadrants() {
        return quadrants;
    }

    public Quadrant getQuadrant(int x, int y) {
        Quadrant result = null;
        for (int i=0; i<8; i++){
            for (int j=0; j<8; j++){
                if(quadrants[i][j].getX() == x){
                    if (quadrants[i][j].getY() == y){
                        result = quadrants[i][j];
                    }
                }
            }
        }
        return result;
    }

    public Quadrant getRandomQuadrant(){
        Random rd = new Random();
        int x;
        int y;

        x = rd.nextInt(7);
        y = rd.nextInt(7);
        Quadrant quadrant = getQuadrant(x,y);
        return quadrant;
    }

    private void createQuadrants(){
        int x = 7;

        for(int i=0; i<8; i++){
            int y=0;
            for (int j=0; j<8; j++){
                quadrants[i][j] = new Quadrant(x, y);
                y++;
            }
            x--;
        }
    }
}
