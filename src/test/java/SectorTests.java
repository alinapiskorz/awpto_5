import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class SectorTests {

    @Test
    public void shouldNotBeEmptyAfterPlacingElement(){
        Sector sector = new Sector(0, 0);

        sector.placeElement(new Star());

        assertThat(sector.isEmpty()).isFalse();
    }

    @Test
    public void shouldReturnTrueIfEmpty(){
        Sector sector = new Sector(0, 0);

        assertThat(sector.isEmpty()).isTrue();
    }

}
