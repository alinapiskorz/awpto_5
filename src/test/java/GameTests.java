import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class GameTests {

    @Test
    public void shouldStartGame(){
        Game game = new Game();

        game.create();

        assertThat(game.isRunning()).isTrue();
    }

    @Test
    public void shouldHave7KlingonsShips(){
        Game game = new Game();
        game.create();

        int klingonsShipsAmount = game.getKlingonsShipsAmount();

        assertThat(klingonsShipsAmount).isEqualTo(7);
    }

    @Test
    public void shouldHave2StarfleetBases(){
        Game game = new Game();
        game.create();

        int starfleetBasesAmount = game.getStarfleetBasesAmount();

        assertThat(starfleetBasesAmount).isEqualTo(2);
    }

    @Test
    public void shouldHave20Stars(){
        Game game = new Game();
        game.create();

        int starsAmount = game.getStarsAmount();

        assertThat(starsAmount).isEqualTo(20);
    }

    @Test
    public void shouldHaveEnterprise(){
        Game game = new Game();

        boolean enterprise = game.haveEnterprise();

        assertThat(enterprise).isTrue();
    }

    @Test
    public void enterpriseShouldBeInTheMiddleOfTheGalaxyMap(){
        Game game = new Game();

        Enterprise enterprise = game.getEnterprise();

        assertThat(enterprise.getQuadrant().getX()).isEqualTo(4);
        assertThat(enterprise.getQuadrant().getY()).isEqualTo(4);
        assertThat(enterprise.getSector().getX()).isEqualTo(4);
        assertThat(enterprise.getSector().getY()).isEqualTo(4);
    }

    @Test
    public void shouldExecuteMove(){
        Game game = new Game();
        Enterprise enterprise = game.getEnterprise();

        game.executeMove(1,0);

        assertThat(enterprise.getSector().getX()).isEqualTo(4);
        assertThat(enterprise.getSector().getY()).isEqualTo(5);
    }

    @Test
    public void shouldConsumeEnergyAfterExecutingMove(){
        Game game = new Game();
        Enterprise enterprise = game.getEnterprise();

        game.executeMove(1,1);

        assertThat(enterprise.getEnergy()).isEqualTo(598);
    }

    @Test
    public void shouldChangeQuadrantAndConsume1StardateAfterMovingOutsideCurrentQuadrant(){
        Game game = new Game();
        Enterprise enterprise = game.getEnterprise();

        game.executeMove(8, 8);

        assertThat(enterprise.getQuadrant().getX()).isEqualTo(5);
        assertThat(enterprise.getQuadrant().getY()).isEqualTo(5);
        assertThat(enterprise.getSector().getX()).isEqualTo(4);
        assertThat(enterprise.getSector().getY()).isEqualTo(4);
        assertThat(enterprise.getStardate()).isEqualTo(28);
    }

    @Test
    public void shouldFinishGameAfterMovingToNotEmptySector(){
        Game game = new Game();
        game.create();
        Sector sector = game.getGalaxyMap().getQuadrant(4,4).getSector(4,5);

        sector.placeElement(new Star());
        game.executeMove(1, 0);

        assertThat(game.isRunning()).isFalse();
    }

    @Test
    public void shouldFinishGameWhenHaving0StardatesAndActiveKlingonsShipsInAnotherQuadrant(){
        Game game = new Game();
        game.create();
        Enterprise enterprise = game.getEnterprise();
        Quadrant quadrant = game.getGalaxyMap().getQuadrant(4,4);

        enterprise.setStardates(1);
        quadrant.getRandomSector().placeElement(new KlingonsShip(quadrant));
        game.executeMove(5,0);

        assertThat(game.isRunning()).isFalse();
    }

    @Test
    public void shouldRestoreEnergyAfterMovingToStarfleetBaseNeighbourSector(){
        Game game = new Game();
        Sector sector = game.getGalaxyMap().getQuadrant(4,4).getSector(4,6);

        sector.placeElement(new StarfleetBase());
        game.executeMove(1, 0);

        assertThat(game.getEnterprise().getEnergy()).isEqualTo(600);
    }

    @Test
    public void shouldFinishGame(){
        Game game = new Game();

        game.finish();

        assertThat(game.isRunning()).isFalse();
    }

}
