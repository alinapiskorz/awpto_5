import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class QuadrantTests {

    @Test
    public void shouldHave64Sectors(){
        Quadrant quadrant = new Quadrant(0,0);

        int sectorsAmount = quadrant.getSectorsAmount();

        assertThat(sectorsAmount).isEqualTo(64);
    }

    @Test
    public void shouldReturnSector00InBottomLeftCorner(){
        Quadrant quadrant = new Quadrant(0,0);

        Sector[][] sectors = quadrant.getSectors();

        assertThat(sectors[7][0].getX()).isEqualTo(0);
        assertThat(sectors[7][0].getY()).isEqualTo(0);
    }

    @Test
    public void shouldReturnSectorWithCoordinates(){
        Quadrant quadrant = new Quadrant(1,1);

        Sector sector = quadrant.getSector(1, 1);

        assertThat(sector.getX()).isEqualTo(1);
        assertThat(sector.getY()).isEqualTo(1);
    }

}
