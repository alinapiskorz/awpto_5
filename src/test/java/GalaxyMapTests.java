import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class GalaxyMapTests {

    @Test
    public void shouldReturnQuadrant00InBottomLeftCorner(){
        GalaxyMap galaxyMap = new GalaxyMap();

        Quadrant[][] quadrants = galaxyMap.getQuadrants();

        assertThat(quadrants[7][0].getX()).isEqualTo(0);
        assertThat(quadrants[7][0].getY()).isEqualTo(0);
    }

    @Test
    public void shouldReturnQuadrantWithCoordinates(){
        GalaxyMap galaxyMap = new GalaxyMap();

        Quadrant quadrant = galaxyMap.getQuadrant(1, 1);

        assertThat(quadrant.getX()).isEqualTo(1);
        assertThat(quadrant.getY()).isEqualTo(1);
    }
}
