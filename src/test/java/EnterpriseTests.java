import org.junit.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class EnterpriseTests {

    @Test
    public void shouldHave30StardatesAfterStart(){
        Enterprise enterprise = new Enterprise(new Quadrant(4,4), new Sector(4, 4));

        int stardate = enterprise.getStardate();

        assertThat(stardate).isEqualTo(30);
    }

    @Test
    public void shouldHave600EnergyAfterStart(){
        Enterprise enterprise = new Enterprise(new Quadrant(4,4), new Sector(4, 4));

        int energy = enterprise.getEnergy();

        assertThat(energy).isEqualTo(600);
    }
}
