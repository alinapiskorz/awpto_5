import org.junit.Test;
import static org.assertj.core.api.Assertions.assertThat;

public class KlingonsShipTests {

    @Test
    public void shouldHave550EnergyAfterStart(){
        KlingonsShip klingonsShip = new KlingonsShip(new Quadrant(1, 1));

        int energy = klingonsShip.getEnergy();

        assertThat(energy).isEqualTo(550);
    }
}
